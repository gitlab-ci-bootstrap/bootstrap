#!/bin/bash

import "single-import-from-remote"
import "multiple-import-from-remote"
import "nested-import-from-remote"
import "multiple-import-from-remote"

include "tests/includes/single-include-from-remote.sh"
include "tests/includes/multiple-include-from-remote.sh"
include "tests/includes/nested-include-from-remote.sh"
include "tests/includes/multiple-include-from-remote.sh"
include "tests/includes/hook-include-from-remote.sh"

################################################################################
## VARIABLES
################################################################################

LOADING_SCRIPTS_FROM_REMOTE_REPOSITORY=$((LOADING_SCRIPTS_FROM_REMOTE_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_echo_task() {
    echo_gray "This is a gray message."
    echo_red "This is a red message."
    echo_green "This is a green message."
    echo_yellow "This is a yellow message."
    echo_blue "This is a blue message."
    echo_magenta "This is a magenta message."
    echo_cyan "This is a cyan message."
    echo

    echo_error "This is a short error message."
    echo_error \
        "This is a very long" \
        "error message"
    echo

    echo_warning "This is a short warning message."
    echo_warning \
        "This is a very long" \
        "warning message"
    echo
}

function test_section_task() {
    section_start "This is a first root section."
    echo "This is a message inside the first root section."

    section_start "This is a first nested section."
    echo "This is a message inside the first nested section."
    section_end

    section_start "This is a second nested section."
    echo "This is a message inside the second nested section."
    section_end

    section_end
    echo

    section_start "This is a second root section."
    echo "This is a message inside the second root section."
    section_end
    echo
}

function test_path_from_remote_repository_task() {
    local project="$CI_PROJECT_PATH"
    local ref="$CI_COMMIT_REF_NAME"
    local file="tests/sample.txt"

    local expected_path="$BOOTSTRAP_CLONE_DIR/$project/-/$ref/-/$file"
    local actual_path=$(path "$file")

    echo "expected_path: $expected_path"
    echo "actual_path: $actual_path"
    echo

    [ "$expected_path" == "$actual_path" ] || exit 1

    local expected_text="SAMPLE"
    local actual_text=$(cat "$actual_path")

    echo "expected_text: $expected_text"
    echo "actual_text: $actual_text"
    echo

    [ "$expected_text" == "$actual_text" ] || exit 1
}

function test_loading_scripts_from_remote_repository_task() {
    echo "LOADING_SCRIPTS_FROM_REMOTE_REPOSITORY: $LOADING_SCRIPTS_FROM_REMOTE_REPOSITORY"
    echo

    [ $((LOADING_SCRIPTS_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}

function test_before_hooks_task() {
    echo "BEFORE_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY: $BEFORE_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY"
    echo "BEFORE_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY: $BEFORE_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo
    echo "AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY: $AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY"
    echo "AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY: $AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo

    [ $((BEFORE_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
    [ $((BEFORE_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1

    [ $((AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY)) -eq 0 ] || exit 1
    [ $((AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 0 ] || exit 1
}

function test_after_hooks_task() {
    echo "AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY: $AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY"
    echo "AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY: $AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo

    [ $((AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
    [ $((AFTER_HOOK_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}
