#!/bin/bash

################################################################################
## VARIABLES
################################################################################

MULTIPLE_IMPORT_FROM_LOCAL_REPOSITORY=$((MULTIPLE_IMPORT_FROM_LOCAL_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_multiple_import_from_local_repository_task() {
    echo "MULTIPLE_IMPORT_FROM_LOCAL_REPOSITORY: $MULTIPLE_IMPORT_FROM_LOCAL_REPOSITORY"
    echo

    [ $((MULTIPLE_IMPORT_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
}
