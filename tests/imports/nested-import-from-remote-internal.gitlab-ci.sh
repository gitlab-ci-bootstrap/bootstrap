#!/bin/bash

################################################################################
## VARIABLES
################################################################################

NESTED_IMPORT_FROM_REMOTE_REPOSITORY=$((NESTED_IMPORT_FROM_REMOTE_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_nested_import_from_remote_repository_task() {
    echo "NESTED_IMPORT_FROM_REMOTE_REPOSITORY: $NESTED_IMPORT_FROM_REMOTE_REPOSITORY"
    echo

    [ $((NESTED_IMPORT_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}
