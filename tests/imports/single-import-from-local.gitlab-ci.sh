#!/bin/bash

################################################################################
## VARIABLES
################################################################################

SINGLE_IMPORT_FROM_LOCAL_REPOSITORY=$((SINGLE_IMPORT_FROM_LOCAL_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_single_import_from_local_repository_task() {
    echo "SINGLE_IMPORT_FROM_LOCAL_REPOSITORY: $SINGLE_IMPORT_FROM_LOCAL_REPOSITORY"
    echo

    [ $((SINGLE_IMPORT_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
}
