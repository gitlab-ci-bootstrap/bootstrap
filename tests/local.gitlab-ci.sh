#!/bin/bash

import "single-import-from-local"
import "multiple-import-from-local"
import "nested-import-from-local"
import "multiple-import-from-local"

include "tests/includes/single-include-from-local.sh"
include "tests/includes/multiple-include-from-local.sh"
include "tests/includes/nested-include-from-local.sh"
include "tests/includes/multiple-include-from-local.sh"
include "tests/includes/hook-include-from-local.sh"

################################################################################
## VARIABLES
################################################################################

LOADING_SCRIPTS_FROM_LOCAL_REPOSITORY=$((LOADING_SCRIPTS_FROM_LOCAL_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_path_from_local_repository_task() {
    local file="tests/sample.txt"

    local expected_path="$file"
    local actual_path=$(path "$file")

    echo "expected_path: $expected_path"
    echo "actual_path: $actual_path"
    echo

    [ "$expected_path" == "$actual_path" ] || exit 1

    local expected_text="SAMPLE"
    local actual_text=$(cat "$actual_path")

    echo "expected_text: $expected_text"
    echo "actual_text: $actual_text"
    echo

    [ "$expected_text" == "$actual_text" ] || exit 1
}

function test_loading_scripts_from_local_repository_task() {
    echo "LOADING_SCRIPTS_FROM_LOCAL_REPOSITORY: $LOADING_SCRIPTS_FROM_LOCAL_REPOSITORY"
    echo

    [ $((LOADING_SCRIPTS_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
}
