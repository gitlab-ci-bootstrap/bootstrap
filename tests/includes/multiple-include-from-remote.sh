#!/bin/bash

################################################################################
## VARIABLES
################################################################################

MULTIPLE_INCLUDE_FROM_REMOTE_REPOSITORY=$((MULTIPLE_INCLUDE_FROM_REMOTE_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_multiple_include_from_remote_repository_task() {
    echo "MULTIPLE_INCLUDE_FROM_REMOTE_REPOSITORY: $MULTIPLE_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo

    [ $((MULTIPLE_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}
