#!/bin/bash

################################################################################
## VARIABLES
################################################################################

SINGLE_INCLUDE_FROM_REMOTE_REPOSITORY=$((SINGLE_INCLUDE_FROM_REMOTE_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_single_include_from_remote_repository_task() {
    echo "SINGLE_INCLUDE_FROM_REMOTE_REPOSITORY: $SINGLE_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo

    [ $((SINGLE_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}
