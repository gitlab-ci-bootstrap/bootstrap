#!/bin/bash

################################################################################
## VARIABLES
################################################################################

SINGLE_INCLUDE_FROM_LOCAL_REPOSITORY=$((SINGLE_INCLUDE_FROM_LOCAL_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_single_include_from_local_repository_task() {
    echo "SINGLE_INCLUDE_FROM_LOCAL_REPOSITORY: $SINGLE_INCLUDE_FROM_LOCAL_REPOSITORY"
    echo

    [ $((SINGLE_INCLUDE_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
}
