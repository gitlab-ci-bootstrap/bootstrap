#!/bin/bash

################################################################################
## VARIABLES
################################################################################

NESTED_INCLUDE_FROM_REMOTE_REPOSITORY=$((NESTED_INCLUDE_FROM_REMOTE_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_nested_include_from_remote_repository_task() {
    echo "NESTED_INCLUDE_FROM_REMOTE_REPOSITORY: $NESTED_INCLUDE_FROM_REMOTE_REPOSITORY"
    echo

    [ $((NESTED_INCLUDE_FROM_REMOTE_REPOSITORY)) -eq 1 ] || exit 1
}
