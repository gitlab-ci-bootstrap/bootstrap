#!/bin/bash

################################################################################
## FUNCTIONS
################################################################################

function before_test_before_hooks_task() {
    BEFORE_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY=$((BEFORE_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY + 1))
}

function after_test_before_hooks_task() {
    AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY=$((AFTER_HOOK_INCLUDE_FROM_LOCAL_REPOSITORY + 1))
}
