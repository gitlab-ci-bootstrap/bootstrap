#!/bin/bash

################################################################################
## VARIABLES
################################################################################

NESTED_INCLUDE_FROM_LOCAL_REPOSITORY=$((NESTED_INCLUDE_FROM_LOCAL_REPOSITORY + 1))

################################################################################
## FUNCTIONS
################################################################################

function test_nested_include_from_local_repository_task() {
    echo "NESTED_INCLUDE_FROM_LOCAL_REPOSITORY: $NESTED_INCLUDE_FROM_LOCAL_REPOSITORY"
    echo

    [ $((NESTED_INCLUDE_FROM_LOCAL_REPOSITORY)) -eq 1 ] || exit 1
}
