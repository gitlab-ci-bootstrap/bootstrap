#!/bin/bash

################################################################################
## CONSTANTS
################################################################################

OUTPUT_PATH="tests.gitlab-ci.yml"

################################################################################
## FUNCTIONS
################################################################################

function generate_tests_task() {
    echo_blue "Generating '$OUTPUT_PATH' file..."
    echo

    generate_tests |
        tee "$OUTPUT_PATH" |
        sed -e 's/^/  /'
    echo
}

function generate_tests() {
    local project="$CI_PROJECT_PATH"
    local ref="$CI_COMMIT_REF_NAME"

    echo "include:"
    echo ""
    echo "  - local: 'tests/local.gitlab-ci.yml'"
    echo ""
    echo "  - project: '$project'"
    echo "    ref: '$ref'"
    echo "    file: 'tests/remote.gitlab-ci.yml'"
    echo ""

    for file in $(find tests/imports -name '*.gitlab-ci.yml'); do
        echo "  - project: '$project'"
        echo "    ref: '$ref'"
        echo "    file: '$file'"
        echo ""
    done

    echo "variables:"
    echo "  CI_CONFIG_PATH: 'tests/local.gitlab-ci.yml'"
    echo ""
}
