#!/bin/bash

################################################################################
## CONSTANTS
################################################################################

BOOTSTRAP_CLONE_DIR=".gitlab-ci"

################################################################################
## UTILITIES
################################################################################

function echo() {
    [ $# -gt 0 ] && command echo "$@" || command echo " "
}

function echo_gray() {
    command echo -e "\e[0;37m$@\e[0;39m"
}
function echo_red() {
    command echo -e "\e[0;91m$@\e[0;39m"
}
function echo_green() {
    command echo -e "\e[0;92m$@\e[0;39m"
}
function echo_yellow() {
    command echo -e "\e[0;93m$@\e[0;39m"
}
function echo_blue() {
    command echo -e "\e[0;94m$@\e[0;39m"
}
function echo_magenta() {
    command echo -e "\e[0;95m$@\e[0;39m"
}
function echo_cyan() {
    command echo -e "\e[0;96m$@\e[0;39m"
}

function echo_error() {
    command echo -e "\e[0;91mERROR: $@\e[0;39m"
}
function echo_warning() {
    command echo -e "\e[0;93mWARNING: $@\e[0;39m"
}

function echo_debug() {
    if [ -n "$DEBUG" ]; then
        command echo -e "\e[0;90mDEBUG: $@\e[0;39m"
    fi
}

function section_start() {
    if [ -n "$BOOTSTRAP_SECTION" ]; then
        BOOTSTRAP_SECTION_DEPTH=$((BOOTSTRAP_SECTION_DEPTH + 1))
        eval 'BOOTSTRAP_SECTION_'$BOOTSTRAP_SECTION_DEPTH'="'$BOOTSTRAP_SECTION'"'
    fi

    BOOTSTRAP_SECTION_COUNTER=$((BOOTSTRAP_SECTION_COUNTER + 1))
    BOOTSTRAP_SECTION="$BOOTSTRAP_SECTION_COUNTER"

    echo -e "\e[0Ksection_start:$(date +%s):${BOOTSTRAP_SECTION}[collapsed=true]\r\e[0K$@"
}

function section_end() {
    echo -e "\e[0Ksection_end:$(date +%s):${BOOTSTRAP_SECTION}\r\e[0K"

    if [ $((BOOTSTRAP_SECTION_DEPTH)) -gt 0 ]; then
        BOOTSTRAP_SECTION=$(eval 'echo $BOOTSTRAP_SECTION_'$BOOTSTRAP_SECTION_DEPTH)
        eval 'unset BOOTSTRAP_SECTION_'$BOOTSTRAP_SECTION_DEPTH
        BOOTSTRAP_SECTION_DEPTH=$((BOOTSTRAP_SECTION_DEPTH - 1))
    else
        unset BOOTSTRAP_SECTION
        unset BOOTSTRAP_SECTION_DEPTH
    fi
}

################################################################################
## FUNCTIONS
################################################################################

function bootstrap() {
    bootstrap_remote
    bootstrap_local
    bootstrap_clean
}

function bootstrap_remote() {
    BOOTSTRAP_MODE="remote"

    local project="$CI_BOOTSTRAP_PROJECT"
    local ref="$CI_BOOTSTRAP_REF"
    local file="$CI_BOOTSTRAP_FILE"

    if [ -n "$project" ] || [ -n "$ref" ] || [ -n "$file" ]; then
        import "bootstrap"
    fi
}

function bootstrap_local() {
    BOOTSTRAP_MODE="local"

    local file_path=$(dirname "$CI_CONFIG_PATH")
    local file_name=$(basename "$CI_CONFIG_PATH" ".yml")
    local file=$([ "$file_path" == "." ] && echo "$file_name.sh" || echo "$file_path/$file_name.sh")

    if [ -f "$file" ]; then
        include "$file"
    fi
}

function bootstrap_clean() {
    unset "BOOTSTRAP_MODE"
    unset "BOOTSTRAP_IMPORTS"
    unset "BOOTSTRAP_INCLUDES"
}

function import() {
    if [ $# -eq 1 ]; then

        local module="$1"
        local module_variable="$(echo "$module" | tr 'a-z\-' 'A-Z_')"

        import_check "$module" || return 0

        local project_variable="CI_${module_variable}_PROJECT"
        local project="$(eval 'echo $'$project_variable)"

        if [ -z "$project" ]; then
            echo_error "Failed to import '$module' module. No '$project_variable' variable defined."
            echo && exit 1
        fi

        local ref_variable="CI_${module_variable}_REF"
        local ref="$(eval 'echo $'$ref_variable)"

        if [ -z "$ref" ]; then
            echo_error "Failed to import '$module' module. No '$ref_variable' variable defined."
            echo && exit 1
        fi

        local file_variable="CI_${module_variable}_FILE"
        local file=$(eval 'echo $'$file_variable)

        if [ -z "$file" ]; then
            echo_error "Failed to import '$module' module. No '$file_variable' variable defined."
            echo && exit 1
        fi

        include "$project" "$ref" "$file"

    else
        echo_error "Failed to import module. Invalid argument count."
        echo && exit 1
    fi
}

function import_check() {
    local module="$1"

    if [ -z "$BOOTSTRAP_IMPORTS" ]; then
        BOOTSTRAP_IMPORTS="$module"
        return 0
    fi

    if [ -z "$(echo "$BOOTSTRAP_IMPORTS" | grep "^$module\$")" ]; then
        BOOTSTRAP_IMPORTS=$(echo "$BOOTSTRAP_IMPORTS" && command echo "$module")
        return 0
    fi

    return 1
}

function include() {
    if [ $# -eq 1 ]; then

        local file="$1"

        if [ "$BOOTSTRAP_MODE" == "remote" ]; then
            include_remote "$file"
        elif [ "$BOOTSTRAP_MODE" == "local" ]; then
            include_local "$file"
        fi

    elif [ $# -eq 3 ]; then

        local project="$1"
        local ref="$2"
        local file="$3"

        clone "$project" "$ref"

        local current_project="$BOOTSTRAP_PROJECT"
        local current_ref="$BOOTSTRAP_REF"

        BOOTSTRAP_PROJECT="$project"
        BOOTSTRAP_REF="$ref"

        if [ "$BOOTSTRAP_MODE" == "remote" ]; then
            include_remote "$file"
        elif [ "$BOOTSTRAP_MODE" == "local" ]; then
            BOOTSTRAP_MODE="remote"
            include_remote "$file"
            BOOTSTRAP_MODE="local"
        fi

        BOOTSTRAP_PROJECT="$current_project"
        BOOTSTRAP_REF="$current_ref"

    else
        echo_error "Failed to include file. Invalid argument count."
        echo && exit 1
    fi
}

function include_remote() {
    local project="$BOOTSTRAP_PROJECT"
    local ref="$BOOTSTRAP_REF"
    local file="$1"

    include_check "$project" "$ref" "$file" || return 0

    local file_path="$CI_PROJECT_DIR/$BOOTSTRAP_CLONE_DIR/$project/-/$ref/-/$file"

    if [ ! -f "$file_path" ]; then
        echo_error "Failed to include '$file' file. File not found."
        echo && exit 1
    fi

    local script=$(cat "$file_path")

    include_eval "$project" "$ref" "$file" "$script"
}

function include_local() {
    local project="$CI_PROJECT_PATH"
    local ref="$CI_COMMIT_REF_NAME"
    local file="$1"

    include_check "$project" "$ref" "$file" || return 0

    local file_path="$CI_PROJECT_DIR/$file"

    if [ ! -f "$file_path" ]; then
        echo_error "Failed to include '$file' file. File not found."
        echo && exit 1
    fi

    local script=$(cat "$file_path")

    include_eval "$project" "$ref" "$file" "$script"
}

function include_check() {
    local project="$1"
    local ref="$2"
    local file="$3"

    if [ -z "$BOOTSTRAP_INCLUDES" ]; then
        BOOTSTRAP_INCLUDES="$project:$ref:$file"
        return 0
    fi

    if [ -z "$(echo "$BOOTSTRAP_INCLUDES" | grep "^$project:$ref:$file\$")" ]; then
        BOOTSTRAP_INCLUDES=$(echo "$BOOTSTRAP_INCLUDES" && echo "$project:$ref:$file")
        return 0
    fi

    return 1
}

function include_eval() {
    local project="$1"
    local ref="$2"
    local file="$3"
    local script="$4"

    local url="https://$CI_SERVER_HOST/$project/-/blob/$ref/$file"

    if [ -n "$DEBUG" ]; then
        echo "$url"
    fi

    local tasks=$(
        echo "$script" |
            grep -E -n -o '^function [a-zA-Z][a-zA-Z0-9_]*' |
            grep -E '_task$' |
            sed -r 's/:function /:/g'
    )

    if [ -z "$tasks" ]; then
        eval "$script"
        return
    fi

    BOOTSTRAP_EVAL_COUNTER=$((BOOTSTRAP_EVAL_COUNTER + 1))

    local counter="$BOOTSTRAP_EVAL_COUNTER"

    if [ -z "$BASH_VERSION" ]; then
        script=$(
            echo "$script" |
                sed -r 's/^function (before_[a-zA-Z][a-zA-Z0-9_]*_task[^a-zA-Z0-9_])/function __bootstrap_origin_'$counter'_\1/g' |
                sed -r 's/^function (after_[a-zA-Z][a-zA-Z0-9_]*_task[^a-zA-Z0-9_])/function __bootstrap_origin_'$counter'_\1/g' |
                sed -r 's/^function ([a-zA-Z][a-zA-Z0-9_]*_task[^a-zA-Z0-9_])/function __bootstrap_origin_\1/g'
        )
    fi

    eval "$script"

    for task in $(echo "$tasks" | grep -E -v ':(before|after)_'); do

        local name=$(echo "$task" | sed -r 's/^(.+?):(.+?)$/\2/g')
        local line=$(echo "$task" | sed -r 's/^(.+?):(.+?)$/\1/g')

        if [ -n "$BASH_VERSION" ]; then
            local func=$(declare -f $name)
            eval "function __bootstrap_origin_${func}"
        fi

        eval '
        function '$name'() {
            local before_hooks=$(echo -e "$BOOTSTRAP_HOOKS" |
                grep -E "^before_'$name':" |
                sed -r "s/^(.+?):(.+?)$/\2/g"
            )

            local after_hooks=$(echo -e "$BOOTSTRAP_HOOKS" |
                grep -E "^after_'$name':" |
                sed -r "s/^(.+?):(.+?)$/\2/g"
            )

            local current_mode="$BOOTSTRAP_MODE"
            local current_project="$BOOTSTRAP_PROJECT"
            local current_ref="$BOOTSTRAP_REF"

            BOOTSTRAP_MODE="'$BOOTSTRAP_MODE'"
            BOOTSTRAP_PROJECT="'$project'"
            BOOTSTRAP_REF="'$ref'"

            if [ -n "$before_hooks" ] || [ -n "$after_hooks" ]; then

                if [ -n "$before_hooks" ]; then
                    for before_hook in $before_hooks; do
                        $before_hook "$@"
                    done
                fi

                if [ -n "$DEBUG" ]; then
                    echo "'$url'#L'$line'"
                fi

                __bootstrap_origin_'$name' "$@"

                if [ -n "$after_hooks" ]; then
                    for after_hook in $after_hooks; do
                        $after_hook "$@"
                    done
                fi

            else

                if [ -n "$DEBUG" ]; then
                    echo "'$url'#L'$line'"
                fi

                __bootstrap_origin_'$name' "$@"

            fi

            BOOTSTRAP_MODE="$current_mode"
            BOOTSTRAP_PROJECT="$current_project"
            BOOTSTRAP_REF="$current_ref"
        }'

    done

    for task in $(echo "$tasks" | grep -E ':(before|after)_'); do

        local name=$(echo "$task" | sed -r 's/^(.+?):(.+?)$/\2/g')
        local line=$(echo "$task" | sed -r 's/^(.+?):(.+?)$/\1/g')

        if [ -n "$BASH_VERSION" ]; then
            local func=$(declare -f $name)
            eval "function __bootstrap_origin_${counter}_${func}"
        else
            eval '
            function '$name'() {
                __bootstrap_origin_'$counter'_'$name' "$@"
            }'
        fi

        eval '
        function __bootstrap_hook_'$counter'_'$name'() {
            local current_mode="$BOOTSTRAP_MODE"
            local current_project="$BOOTSTRAP_PROJECT"
            local current_ref="$BOOTSTRAP_REF"

            BOOTSTRAP_MODE="'$BOOTSTRAP_MODE'"
            BOOTSTRAP_PROJECT="'$project'"
            BOOTSTRAP_REF="'$ref'"

            if [ -n "$DEBUG" ]; then
                echo "'$url'#L'$line'"
            fi

            __bootstrap_origin_'$counter'_'$name' "$@"

            BOOTSTRAP_MODE="$current_mode"
            BOOTSTRAP_PROJECT="$current_project"
            BOOTSTRAP_REF="$current_ref"
        }'

        if [ -z "$BOOTSTRAP_HOOKS" ]; then
            BOOTSTRAP_HOOKS="${name}:__bootstrap_hook_${counter}_${name}"
        else
            BOOTSTRAP_HOOKS=$(echo "$BOOTSTRAP_HOOKS" && echo "${name}:__bootstrap_hook_${counter}_${name}")
        fi

    done
}

function clone() {
    if [ $# -eq 2 ]; then

        local project="$1"
        local ref="$2"

        local clone_url="https://gitlab-ci-token:$CI_JOB_TOKEN@$CI_SERVER_HOST/$project.git"
        local clone_path="$CI_PROJECT_DIR/$BOOTSTRAP_CLONE_DIR/$project/-/$ref/-"

        if [ ! -d "$clone_path" ]; then

            require "git"

            if [ ! -d "$CI_PROJECT_DIR/$BOOTSTRAP_CLONE_DIR" ]; then
                mkdir "$CI_PROJECT_DIR/$BOOTSTRAP_CLONE_DIR"
                echo "*" >"$CI_PROJECT_DIR/$BOOTSTRAP_CLONE_DIR/.gitignore"
            fi

            git clone "$clone_url" "$clone_path" \
                --branch "$ref" \
                --depth 1 \
                --quiet

            rm -rf "$clone_path/.git"

        fi

    else
        echo_error "Failed to clone. Invalid argument count."
        echo && exit 1
    fi
}

function require() {
    if [ $# -eq 1 ]; then

        local package="$1"

        if [ ! -x "$(command -v $package)" ]; then
            if [ -x "$(command -v apk)" ]; then

                echo_warning \
                    "This job requires $package. It will be installed automatically," \
                    "however it is recommended to use a docker image where $package is preinstalled."

                if [ -n "$DEBUG" ]; then

                    section_start "apk update"
                    apk update
                    section_end

                    section_start "apk add $package"
                    apk add "$package"
                    section_end

                else
                    apk update >/dev/null
                    apk add "$package" >/dev/null
                fi

            elif [ -x "$(command -v apt-get)" ]; then

                echo_warning \
                    "This job requires $package. It will be installed automatically," \
                    "however it is recommended to use a docker image where $package is preinstalled."

                if [ -n "$DEBUG" ]; then

                    section_start "apt-get update"
                    apt-get --yes update
                    section_end

                    section_start "apt-get install $package"
                    apt-get --yes install "$package"
                    section_end

                else
                    apt-get --yes update >/dev/null
                    apt-get --yes install "$package" >/dev/null
                fi

            elif [ -x "$(command -v yum)" ]; then

                echo_warning \
                    "This job requires $package. It will be installed automatically," \
                    "however it is recommended to use a docker image where $package is preinstalled."

                if [ -n "$DEBUG" ]; then

                    section_start "yum install $package"
                    yum --assumeyes install "$package"
                    section_end

                else
                    yum --assumeyes install "$package" >/dev/null
                fi

            else
                echo_error "This job requires $package."
                echo && exit 1
            fi

            if [ ! -x "$(command -v $package)" ]; then
                echo_error "Failed to install $package."
                echo && exit 1
            fi
        fi

    else
        echo_error "Failed to install package. Invalid argument count."
        echo && exit 1
    fi
}

function path() {
    local file="$1"

    if [ "$BOOTSTRAP_MODE" == "remote" ]; then
        echo "$BOOTSTRAP_CLONE_DIR/$BOOTSTRAP_PROJECT/-/$BOOTSTRAP_REF/-/$file"
    elif [ "$BOOTSTRAP_MODE" == "local" ]; then
        echo "$file"
    fi
}

################################################################################
## SCRIPT
################################################################################

bootstrap
