GitLab CI Bootstrap
===================

GitLab CI Bootstrap is small framework for GitLab CI designed to simplify complex pipelines development and code reuse.

Table of Contents
-----------------

[[_TOC_]]

Motivation
----------

Usually, pipelines development for GitLab CI comes down to using the `script` keyword, and all scripts are placed in the `.gitlab-ci.yml` file.

It works very well for small pipelines, but as the project grows, their size usually increases, and the `.gitlab-ci.yml` file becomes difficult to understand. In addition, code reuse in this case, as a rule, is just copying scripts from one place to another. Even the `!reference` tag does not improve the situation much.

GitLab CI Bootstrap simplifies the development of complex pipelines by providing developers with the following features:

- Separate the pipeline definition (the `.gitlab-ci.yml` file) and it's scripts (bash or shell) to keep the `.gitlab-ci.yml` file clear and more declarative.

- Split large scripts into smaller ones and allow to include one scripts into another to better structure the project files and make it easier to maintain.

- Move scripts or entire jobs (along with their scripts) to separate repositories to reuse them in different projects.

How to Use
----------

GitLab CI Bootstrap consists of a single `bootstrap.gitlab-ci.yml` file, which must be included in the `.gitlab-ci.yml` file in your project using the `include` keyword. You can do it in several ways:

- For GitLab SaaS, include the file from this repository using the `include:file` keyword:

    ```yml
    include:
      - project: 'gitlab-ci-bootstrap/bootstrap'
        ref: 'main'
        file: 'bootstrap.gitlab-ci.yml'
    ```

- For GitLab Self-Hosted, include the file from this repository using the `include:remote` keyword:

    ```yml
    include:
      - remote: 'https://gitlab.com/gitlab-ci-bootstrap/bootstrap/-/raw/main/bootstrap.gitlab-ci.yml'
    ```

- Copy the file to your project repository and include it using the `include:local` keyword:

    ```yml
    include:
      - local: 'bootstrap.gitlab-ci.yml'
    ```

- Copy the file to your separate repository and include it using `include:file` or `include:remote` as described above.

The first two ways are suitable for a quick start. In other cases, it is recommended to have your own copy of the file.

How It Works
------------

The `bootstrap.gitlab-ci.yml` file contains one single template job named `.bootstrap`. It is responsible for loading and executing scripts from bash or shell files, which can be placed in both local and remote repositories.

Hereinafter, a local repository will be understood as a repository where the pipeline is directly run. Conversely, a remote repository is any other repository in the same GitLab instance.

The `.bootstrap` job looks like this:

```yml
.bootstrap:
  before_script:
    - '...'
```

Other jobs can use the `.bootstrap` job with the `extends` keyword:

```yml
example:
  extends: '.bootstrap'
  script:
    - '...'
```

GitLab CI Bootstrap takes advantage of the fact that GitLab concatenates commands specified with the `before_script` and `script` keywords and executes them together in a single shell. So all variables and functions defined in `before_script` will also be available and can be used in `script`.

The process of loading and executing scripts is described in more detail below.

Loading Scripts From Local Repositories
---------------------------------------

When any job is started, if the `.gitlab-ci.sh` file exists in the local repository, that file will also be loaded and executed. The job will be able to use any variables and call any functions defined in this file. 

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function hello_task() {
            echo "Hello, $GITLAB_USER_NAME!"
        }
        ```

Loading Scripts From Remote Repositories
----------------------------------------

When any job is started, if three variables named `CI_BOOTSTRAP_PROJECT`, `CI_BOOTSTRAP_REF`, and `CI_BOOTSTRAP_FILE` are defined, and these variables refer to the file in the remote repository that file will also be loaded and executed. The job will be able to use any variables and call any functions defined in this file.

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'example/hello-ci'
            ref: 'v1.0'
            file: 'hello.gitlab-ci.yml'
        ```

* Repository: `example/hello-ci` (remote, branch `v1.0`)

    * File: `hello.gitlab-ci.yml`

        ```yml
        variables:
          CI_BOOTSTRAP_PROJECT: 'example/hello-ci'
          CI_BOOTSTRAP_REF: 'v1.0'
          CI_BOOTSTRAP_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `hello.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function hello_task() {
            echo "Hello, $GITLAB_USER_NAME!"
        }
        ```

### Notes

- Loading scripts from the remote repository always happens before loading scripts from the local repository.

- Scripts are loaded from the remote repository by clonning the repository into the `.gitlab-ci` folder with `git`. If `git` is not available, it will be installed through one of available package managers, like as `apk`, `apt-get`, or `yum`.

The `include` Function
----------------------

The `include` function is designed to split large scripts into smaller ones and allow to include one scripts into another to better structure the project files and make it easier to maintain.

When any job is executing, if the `include` function call is occurred, the file specified in the argument of this function will also be loaded and executed. It does not matter in which repository the executable script is located, local or remote. The `include` function always loads and executes a file from the same repository from which it is called.

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'example/hello-ci'
            ref: 'v1.1'
            file: 'hello.gitlab-ci.yml'
        ```

* Repository: `example/hello-ci` (remote, branch `v1.1`)

    * File: `hello.gitlab-ci.yml`

        ```yml
        variables:
          CI_BOOTSTRAP_PROJECT: 'example/hello-ci'
          CI_BOOTSTRAP_REF: 'v1.1'
          CI_BOOTSTRAP_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `hello.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        include "functions/greet.sh"

        function hello_task() {
            greet "$GITLAB_USER_NAME"
        }
        ```

    * File: `functions/greet.sh`

        ```bash
        #!/bin/bash

        function greet() {
            local name="$1"
            local hour=$(date +"%H")

            if [ $hour -ge 5 ] && [ $hour -lt 12 ]; then
                echo "Good morning, $name!"
            elif [ $hour -ge 12 ] && [ $hour -lt 18 ]; then
                echo "Good afternoon, $name!"
            elif [ $hour -ge 18 ] && [ $hour -lt 22 ]; then
                echo "Good evening, $name!"
            else
                echo "Good night, $name!"
            fi
        }
        ```

### Notes

- Each file is loaded and executed only once. If several files contains the `include` function call with the same file, then subsequent calls are ignored.

The `import` Function
---------------------

The `import` function is designed to move scripts to separate repositories to reuse them in different projects.

When any job is executing, if the `import` function call is occurred, if three variables named `CI_{module}_PROJECT`, `CI_{module}_REF`, and `CI_{module}_FILE` are defined and refer to the file in the remote repository, that file will also be loaded and executed. Here `{module}` is the argument of the `import` function, in uppercase with hyphens replaced with underscores.

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'example/hello-ci'
            ref: 'v1.2'
            file: 'hello.gitlab-ci.yml'
        ```

* Repository: `example/hello-ci` (remote, branch `v1.2`)

    * File: `hello.gitlab-ci.yml`

        ```yml
        include:
          - project: 'example/greet-ci'
            ref: 'v1.0'
            file: 'greet.gitlab-ci.yml'

        variables:
          CI_BOOTSTRAP_PROJECT: 'example/hello-ci'
          CI_BOOTSTRAP_REF: 'v1.2'
          CI_BOOTSTRAP_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `hello.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        import "greet"

        function hello_task() {
            greet "$GITLAB_USER_NAME"
        }
        ```

* Repository: `example/greet-ci` (remote, branch `v1.0`)

    * File: `greet.gitlab-ci.yml`

        ```yml
        variables:
          CI_GREET_PROJECT: 'example/greet-ci'
          CI_GREET_REF: 'v1.0'
          CI_GREET_FILE: 'greet.gitlab-ci.sh'
        ```

    * File: `greet.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function greet() {
            local name="$1"
            local hour=$(date +"%H")

            if [ $hour -ge 5 ] && [ $hour -lt 12 ]; then
                echo "Good morning, $name!"
            elif [ $hour -ge 12 ] && [ $hour -lt 18 ]; then
                echo "Good afternoon, $name!"
            elif [ $hour -ge 18 ] && [ $hour -lt 22 ]; then
                echo "Good evening, $name!"
            else
                echo "Good night, $name!"
            fi
        }
        ```

### Notes

- Each module is imported only once. If several files contains the `import` function call with the same module, then subsequent calls are ignored.

Tasks
-----

GitLab CI Bootstrap has two kinds of functions that are handled in a special way.

The first kind of functions is called **tasks**. These are functions whose name ends with the `_task` suffix.

When scripts are loaded and executed, a special wrapper function is created for all tasks in the file. This wrapper is responsible for calling hooks and keep information about repository and ref where task is defined. This information is required for the `path` function correct works.

By convention, tasks are top level functions that should be called directly from the `.gitlab-ci.yml` files. However, if you do not plan to use hooks or the `path` function, their use is optional.

The `hello_task` function from the examples above is an task example.

Hooks
-----

The seconf kind of functions is called **hooks**. These are functions whose name starts with the `before_` or `after_` prefix and ends with the `_task` suffix.

Hooks are functions that are implicitly called before and after the corresponding task.

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'example/hello-ci'
            ref: 'v1.3'
            file: 'hello.gitlab-ci.yml'
        ```

    * File: `.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function before_hello_task() {
            echo "This function will be called before the hello_task function."
        }

        function after_hello_task() {
            echo "This function will be called after the hello_task function."
        }
        ```

* Repository: `example/hello-ci` (remote, branch `v1.3`)

    * File: `hello.gitlab-ci.yml`

        ```yml
        variables:
          CI_BOOTSTRAP_PROJECT: 'example/hello-ci'
          CI_BOOTSTRAP_REF: 'v1.3'
          CI_BOOTSTRAP_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `hello.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function hello_task() {
            echo "Hello, $GITLAB_USER_NAME!"
        }
        ```

The `path` Function
-------------------

The `path` function allow you to get a path to a file placed in the same repository as the executing task.

### Example

* Repository: `example/hello` (local)

    * File: `.gitlab-ci.yml`

        ```yml
        include:
          - local: 'bootstrap.gitlab-ci.yml'

          - project: 'example/hello-ci'
            ref: 'v1.4'
            file: 'hello.gitlab-ci.yml'
        ```

* Repository: `example/hello-ci` (remote, branch `v1.4`)

    * File: `hello.gitlab-ci.yml`

        ```yml
        variables:
          CI_BOOTSTRAP_PROJECT: 'example/hello-ci'
          CI_BOOTSTRAP_REF: 'v1.4'
          CI_BOOTSTRAP_FILE: 'hello.gitlab-ci.sh'

        hello:
          extends: '.bootstrap'
          script:
            - 'hello_task'
        ```

    * File: `hello.gitlab-ci.sh`

        ```bash
        #!/bin/bash

        function hello_task() {
            local path=$(path "assets/message.txt")
            local message=$(cat "$path")
            echo "$message"
        }
        ```

    * File: `assets/message.txt`

        ```
        Hello, World!
        ```

Utilities
---------

GitLab CI Bootstrap also has a set of utility functions that are not directly related with scripts loading, but can also be used:

* The `echo` function is redefined as follows:

    ```bash
    function echo() {
        [ $# -gt 0 ] && command echo "$@" || command echo " "
    }
    ```

  It allows you to output empty lines to job logs, they are not displayed by default.

* There is a set of echo functions for colored output, and error and warning messages:

    ```bash
    echo_gray "This is a gray message."
    echo_red "This is a red message."
    echo_green "This is a green message."
    echo_yellow "This is a yellow message."
    echo_blue "This is a blue message."
    echo_magenta "This is a magenta message."
    echo_cyan "This is a cyan message."
    echo

    echo_error "This is a short error message."
    echo_error \
        "This is a very long" \
        "error message"
    echo

    echo_warning "This is a short warning message."
    echo_warning \
        "This is a very long" \
        "warning message"
    echo
    ```

  ![Echo Output](assets/echo_output.png)

* There are two functions simplify to use [job log sections](https://docs.gitlab.com/ee/ci/jobs/#custom-collapsible-sections):

    ```bash
    section_start "This is a section title."
    echo "This is a message inside the section."
    section_end
    ```
